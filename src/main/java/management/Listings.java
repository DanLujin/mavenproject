package management;

public class Listings {

	private int propertyNumber = 0;
	private String propertyType;
	private double price = 0.0;
	private int agentID = 0;

	public Listings(int propertyNumber, String propertyType, double price, int agentID) {
		this.propertyNumber = propertyNumber;
		this.propertyType = propertyType;
		this.price = price;
		this.agentID = agentID;
	}

	public int getPropertyNumber() {
		return propertyNumber;
	}

	public void setPropertyNumber(int propertyNumber) {
		this.propertyNumber = propertyNumber;
	}

	public String getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getAgentID() {
		return agentID;
	}

	public void setAgentID(int agentID) {
		this.agentID = agentID;
	}

	@Override
	public String toString() {
		return "Listings [propertyNumber=" + propertyNumber + ", propertyType=" + propertyType + ", price=" + price
				+ ", agentID=" + agentID + "]";
	}

}
