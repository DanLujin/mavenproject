package management;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FileCreater {

	public static void fileCreaterMap(Map<Integer, Double> agentReport) throws IOException {

		BufferedWriter bw = null;
		bw = new BufferedWriter(
				new FileWriter("C:\\Users\\lujin\\eclipse-workspace\\management\\agentReport.txt", true));
		for (Integer key : agentReport.keySet()) {
			bw.write("key: " + key + ", value: " + agentReport.get(key));
			bw.newLine();
		}
		bw.flush();
		bw.close();
	}

	public static void fileCreaterSet(Set<String> propertyType) throws IOException {
// this is another way to write these two classes - BufferWriter and FileWriter
		BufferedWriter bw = null;
		FileWriter fw = null;
		final String FILENAME = "C:\\Users\\lujin\\eclipse-workspace\\management\\agentReport.txt";
		fw = new FileWriter(FILENAME, true);
		bw = new BufferedWriter(fw);

		for (String property : propertyType) {
			bw.write(property);
			bw.newLine();
		}

		bw.flush();
		bw.close();
	}

	public static void fileCalculateTotalValue(Map<Integer, Double> agentReport) throws IOException {
		Double sum = 0d;
		BufferedWriter bw = null;
		bw = new BufferedWriter(new FileWriter("C:\\Users\\lujin\\eclipse-workspace\\management\\overview.txt", true));
		for (Double value : agentReport.values()) {
			sum += value;
		}
		BigDecimal bd = new BigDecimal(sum);
		bw.write("Total value of properties listed:" + bd);
		bw.newLine();
		bw.flush();
		bw.close();
		;
	}

	public static void fileCreaterPropertyId(List<Integer> propertyIdList) throws IOException {

		BufferedWriter bw = null;
		bw = new BufferedWriter(new FileWriter("C:\\Users\\lujin\\eclipse-workspace\\management\\overview.txt", true));
		bw.write("Total properties listed: " + propertyIdList.size());
		bw.newLine();
		for (Integer propertyId : propertyIdList) {
			bw.write(propertyId.toString());
			bw.newLine();
		}
		bw.flush();
		bw.close();
		;
	}

	public static void fileClener(String fileName) throws IOException {
		BufferedWriter bw = null;
		bw = new BufferedWriter(
				new FileWriter("C:\\Users\\lujin\\eclipse-workspace\\management\\" + fileName + ".txt"));
		bw.write(" ");
		bw.newLine();
		bw.flush();
		bw.close();

	}

	public static void clearScreen() {
		System.out.print("\033[H\033[2J");
		System.out.flush();
	}

}
