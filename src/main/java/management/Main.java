package management;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class Main {

	public static void main(String[] args) throws IOException {

		// Verify if the file name is correct
		String fileName = FileNameValidator.fileNameValidator();
		// Create an ArrayList to store the objects of type Listings
		List<Listings> objListings = new ArrayList<Listings>();
		// Add all objects created in FileContentReader
		try {
			objListings.addAll(FileContentReader.readFromFile(fileName));
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Create a TreeSet to store the types of properties
		Set<String> propertyTypeSet = new TreeSet<String>();
		propertyTypeSet = ListProcesor.storePropertyToSet(objListings);

//		System.out.println("All property types:");
//		for (String propertyType : propertyTypeSet) {
//			System.out.println(propertyType);
//		}

		// create a map with
		// key - agentID
		// value - how much an agent has sold
		Map<Integer, Double> propertiesPrice = new HashMap<Integer, Double>();
		propertiesPrice = ListProcesor.calculatePricePerIDShortestEver(objListings);

//		System.out.println("Agent - total listes values:");
//		for (Integer key : propertiesPrice.keySet()) {
//			System.out.println("key: " + key + ", value: " + propertiesPrice.get(key));
//		}

		// clean a file
		FileCreater.fileClener("overview");
		FileCreater.fileClener("agentReport");

		FileCreater.fileCreaterMap(propertiesPrice);
		FileCreater.fileCreaterSet(propertyTypeSet);

		// Store each property id into an ArrayList
		List<Integer> propertyIdList = new ArrayList<Integer>();
		propertyIdList = ListProcesor.storePropertyIdToArrayList(objListings);
		ListProcesor.sortPropertyIdArrayList(propertyIdList);
//		System.out.println("Print the list: ");
//		for (Integer propertyId : propertyIdList) {
//			System.out.println(propertyId);
//		}

		// 5.c - use a different collection for this.
		Set<Integer> propertyIdSet = new TreeSet<Integer>();
		propertyIdSet = ListProcesor.storePropertyIdToTreeSet(objListings);
		ListProcesor.printPropertyIdTreeSet(propertyIdSet);

		FileCreater.fileCalculateTotalValue(propertiesPrice);
		FileCreater.fileCreaterPropertyId(propertyIdList);

	}
}
