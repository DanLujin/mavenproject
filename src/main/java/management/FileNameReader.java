package management;

import java.util.Scanner;

public class FileNameReader {

	public static String fileNameReader() {
		// read the name from console
		Scanner scan = new Scanner(System.in);
		String fileName = scan.nextLine();
		return fileName;
	}
}
