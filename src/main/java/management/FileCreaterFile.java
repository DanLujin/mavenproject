package management;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

public class FileCreaterFile {

	public static void fileCreaterFile() throws IOException {
		// use this statement to create a file
		File file = new File("C:\\Users\\lujin\\eclipse-workspace\\management\\agentReportFile.txt");

		// true - if the file doesn't exist
		// false - if the file already exist
		if (file.createNewFile()) {
			System.out.println("File is created!");
		} else {
			System.out.println("File already exists.");
		}

		// Write Content
		FileWriter writer = new FileWriter(file);
		writer.write("Test data");
		writer.close();

	}

	// create a file using java.io.FileOutputStream class
	// write() - method automatically create a new file and write content to it.
	// you can only write bytes type data
	public static void fileCreaterStream() throws IOException {
		String path = "C:\\Users\\lujin\\eclipse-workspace\\management\\OutputStreamExample.txt";
		String data = "Test data";
		FileOutputStream out = new FileOutputStream(path);
		out.write(data.getBytes());
		out.close();
	}

	public static void fileCreaterAppendTest() throws IOException {
		// this is another way to write these two classes - BufferWriter and FileWriter
		BufferedWriter bw = null;
		FileWriter fw = null;
		final String FILENAME = "C:\\Users\\lujin\\eclipse-workspace\\management\\agentReportTest.txt";
		// set it true for append mode
		fw = new FileWriter(FILENAME, true);
		bw = new BufferedWriter(fw);

		bw.write("ana are mere");
		bw.newLine();

		bw.flush();
		bw.close();
	}
	
	

}