package management;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class ListProcesor {

	// 5. Store each property id into an ArrayList
	public static List<Integer> storePropertyIdToArrayList(List<Listings> objListings) {
		List<Integer> propertyIdList = new ArrayList<Integer>();
		for (Listings list : objListings) {
			propertyIdList.add(list.getPropertyNumber());
		}
		return propertyIdList;
	}

	// 5.a Sort the ArrayList using natural ordering
	public static List<Integer> sortPropertyIdArrayList(List<Integer> propertyId) {
		Collections.sort(propertyId);
		return propertyId;
	}

	// 5.c Use a diffrent collection to store the propertyId
	public static Set<Integer> storePropertyIdToTreeSet(List<Listings> objListings) {
		Set<Integer> propertyIdSet = new TreeSet<Integer>();
		for (Listings list : objListings) {
			propertyIdSet.add(list.getPropertyNumber());
		}
		return propertyIdSet;

	}
	// print a TreeSet of Integer
	public static void printPropertyIdTreeSet(Set<Integer> objStoreInSet) {
		for (Integer set : objStoreInSet) {
			System.out.println(set);
		}
	}
	
	// print an ArrayList of Integer
	public static void printPropertyIdArrayList(Set<Integer> objStoreInList) {
		for (Integer list : objStoreInList) {
			System.out.println(list);
		}
	}

	// 3. Store each property into a Set.
	// 3.a. Concert the name to UpperCase
	// 3.b. Sort your Set - by using a TreeSet they are Sorter, because they are of
	// type String
	public static Set<String> storePropertyToSet(List<Listings> objListings) {
		Set<String> propertyTypeSet = new TreeSet<String>();
		for (Listings list : objListings) {
			propertyTypeSet.add((list).getPropertyType().toUpperCase());
		}
		return propertyTypeSet;
	}

	// 4. Use a map to calculate the total property listed in dollars for each agent
	// id.
	// Note: Agent if would be the key, and the accumulated total of property listed
	// would be the value.
	// Step 1: Create a map<Integer, Double>
	// Step 2: Iterate to the list of Listings objects
	// Step 3. Check if id is in the map
	// Step 3.1. : if is not present - add it
	// Step 3.2. : if is present - add it, and get the current value + the new value
	public static Map<Integer, Double> calculatePricePerIDShortestEver(List<Listings> objListings) {
		Map<Integer, Double> soldById = new HashMap<Integer, Double>();
		for (Listings list : objListings) {
			if (soldById.get(list.getAgentID()) == null) {
				soldById.put(list.getAgentID(), list.getPrice());
			} else {
				soldById.put(list.getAgentID(), (soldById.get(list.getAgentID()) + list.getPrice()));
			}
		}
		return soldById;
	}

	// 4. Use a map to calculate the total property listed in dollars for each agent
	// id.
	public static Map<Integer, Double> calculatePricePerIDShortest(List<Listings> objListings) {
		Map<Integer, Double> soldById = new HashMap<Integer, Double>();
		for (Listings list : objListings) {
			// putIfAbsent returns null is is not prezent in the map, of the current value
			// if is already prezent.
			if (soldById.putIfAbsent(list.getAgentID(), list.getPrice()) == null) {
				soldById.put(list.getAgentID(), list.getPrice());
			} else {
				soldById.put(list.getAgentID(),
						(list.getPrice() + soldById.putIfAbsent(list.getAgentID(), list.getPrice())));

			}
		}
		return soldById;
	}

	// 4. Use a map to calculate the total property listed in dollars for each agent
	// id.
	public static Map<Integer, Double> calculatePricePerIDShort(List<Listings> objListings) {
		Map<Integer, Double> soldById = new HashMap<Integer, Double>();
		Set<Integer> agentID = new TreeSet<Integer>();
		// Iterate the List to create a Set of id's
		for (Listings list : objListings) {
			agentID.add(list.getAgentID());
		}
		// Iterate through the Set and thru list and check if the id is already added
		// in case if it's added add the current price to the old price.
		for (Integer setID : agentID) {
			soldById.put(setID, 0.0D);
			for (Listings list : objListings) {
				if (setID == list.getAgentID()) {
					soldById.put(setID, (soldById.get(setID) + list.getPrice()));
				}
			}
		}
		return soldById;
	}

	// 4. Use a map to calculate the total property listed in dollars for each agent
	// id.
	public static Map<Integer, Double> calculatePricePerIDLong(List<Listings> objListings) {
		Map<Integer, Double> soldById = new HashMap<Integer, Double>();
		Set<Integer> agentID = new TreeSet<Integer>();
		// Iterate the List to create a Set of id's
		for (Listings list : objListings) {
			agentID.add(list.getAgentID());
		}
		// Create an array to store the values
		double[] values = new double[agentID.size()];
		for (int i = 0; i < values.length; i++) {
			values[i] = 0;
		}
		int i = 0;
		// Iterate both the list and set to calculate how much each agent has sold
		for (Integer setID : agentID) {
			for (Listings list : objListings) {
				if (list.getAgentID() == setID) {
					values[i] += list.getPrice();
				}
			}
			i++;
		}
		i = 0;
		// Create the map with the Set of id's and the values calulated above
		for (Integer agent : agentID) {
			soldById.put(agent, values[i]);
			i++;
		}
		return soldById;
	}
}
