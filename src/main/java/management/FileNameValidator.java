package management;

public class FileNameValidator {

	public static String fileNameValidator() {
		String fileNameReader;
		boolean isFileName = false;
		do {
			System.out.println("Input file name:");
			fileNameReader = FileNameReader.fileNameReader();
			if (fileNameReader.equals("listings")) {
				isFileName = true;
			} else {
				System.out.println("Wrong Input, try again!");
			}
			
		} while (isFileName == false);
		return fileNameReader;
	}

}
