package management;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileContentReader {

	public static List<Listings> readFromFile(String fileName) throws Exception {
		// Create a file
		File file = new File("C:\\Users\\lujin\\eclipse-workspace\\management\\" + fileName + ".txt");
		
		Scanner scan = new Scanner(file);
		// Create an ArrayList to store the information
		List<Listings> readLineFromFile = new ArrayList<Listings>();
		String[] lineFromFile = new String[4];

		while (scan.hasNextLine()) {
			String line = scan.nextLine();
			// Separate the information from file
			lineFromFile = line.split(" ");
			// Create objects with the data from the file
			Listings listObj = new Listings(Integer.parseInt(lineFromFile[0]), lineFromFile[1],
					 Double.parseDouble(lineFromFile[2]),Integer.parseInt(lineFromFile[3]));
			// Add the objects to the list
			readLineFromFile.add(listObj);

		}
		scan.close();
		return readLineFromFile;

	}
}
